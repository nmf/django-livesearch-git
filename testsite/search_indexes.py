from haystack.indexes import *
from haystack import site

from .models import Content


class ContentIndex(SearchIndex):
    text = CharField(document=True, use_template=True)
    rendered = CharField(indexed=False, use_template=True)
    rendered_live = CharField(indexed=False, use_template=True)
    rendered_inline = CharField(indexed=False, use_template=True)

site.register(Content, ContentIndex)
