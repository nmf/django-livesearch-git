from django.db.models import permalink, Model, CharField, TextField

# Create your models here.
class Content(Model):
    title = CharField(max_length=50)
    body = TextField()

    def __unicode__(self):
        return self.title
        
    @permalink
    def get_absolute_url(self):
        return ('content_detail', (), {'object_id': str(self.id)} )
