from django import template
register = template.Library()

#http://w.holeso.me/2008/08/a-simple-django-truncate-filter/
@register.filter("truncate_chars")
def truncate_chars(value, max_length, ellipsis=True):
    if len(value) > max_length:
        truncd_val = value[:max_length]
        if value[max_length] != " ":
            truncd_val = truncd_val[:truncd_val.rfind(" ")]
        if ellipsis:
            return  truncd_val + "..."
        else:
            return  truncd_val
    return value
    
@register.filter("truncate_chars_html")
def truncate_chars_html(value, max_length):
    return truncate_chars(value, max_length, False)