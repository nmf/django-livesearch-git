from django.conf.urls.defaults import *
from .models import Content

contents = {
    'queryset': Content.objects.all(),
    'template_name': 'content_detail.html'
}


urlpatterns = patterns('',
    (r'^(?P<object_id>\d+)/', 'django.views.generic.list_detail.object_detail', contents, 'content_detail'),
)
