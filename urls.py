from django.conf.urls.defaults import *
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from haystack.views import SearchView
from haystack.forms import HighlightedSearchForm

urlpatterns = patterns('',
    # Example:
     (r'^content/', include('testsite.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    (r'^admin/', include(admin.site.urls)),
    
    # Uncomment the next line to enable haystack search:
    (r'^search/', include('haystack.urls')),

)

# With threading...
urlpatterns += patterns('haystack.views',
    url(r'^livesearch/$',  SearchView(
        template='livesearch_results.html',
        form_class=HighlightedSearchForm,
    ), name='haystack_livesearch'),
    url(r'^livesearch-inline/$',  SearchView(
        template='livesearch_results_inline.html',
        form_class=HighlightedSearchForm,
    ), name='haystack_livesearch'),
)

if settings.DEBUG:
    MEDIA_PREFIX = settings.MEDIA_URL.lstrip('/')
    urlpatterns += patterns('',
        (r'^%s(?P<path>.*)$' % MEDIA_PREFIX, 'django.views.static.serve', 
            {'document_root': '%s' % settings.MEDIA_ROOT}),
    )